# Training notebooks for R and python  

### Environment  
Best to use the installed environment:  
```source activate R-notebook```  

If you need to install it then  
```conda create --name myenv --file env/spec-file.txt```  

### Running these
Use jupyter-notebooks and open up the page.  
```jupyter-notebook```   

### List of notebooks  
```Formula functions in R.ipynb``` Contains a datacamp tutorial for
understanding R data structures and formulias (good for deseq2 training).  
